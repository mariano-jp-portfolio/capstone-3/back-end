// Category Schema
const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
	categoryType: {
		type: String,
		required: [true, 'Category type is required.'] // categoryType can only be 'Income' or 'Expense'
	},
	categoryName: {
		type: String,
		required: [true, 'Category name is required.']
	},
	userId: {
		type: String,
		required: [true, 'User ID is required.']
	},
	records: [
		{
			description: {
				type: String,
				required: [true, 'Record description is required.']
			},
			amount: {
				type: Number,
				required: [true, 'Record amount is required.']
			},
			createdOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model('category', categorySchema);