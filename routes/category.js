// Category Router
const express = require('express');
const router = express.Router();

// Authentication
const auth = require('../auth');

// Controller
const CategoryController = require('../controllers/category');

// ROUTES

// To create or add new category
router.post('/', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		categoryType: req.body.categoryType,
		categoryName: req.body.categoryName
	};
	
	CategoryController.add(params).then(result => res.send(result));
});

// To create or add new records
router.post('/create-record', (req, res) => {
	const params = {
		description: req.body.description,
		amount: req.body.amount,
		categoryId: req.body.categoryId
	};
	
	CategoryController.addRecord(params).then(result => res.send(result));
})

// To fetch all of the user's categories
router.get('/', auth.verify, (req, res) => {
	const params = { userId: auth.decode(req.headers.authorization).id };
	
	CategoryController.getAll(params).then(categories => res.send(categories));
});

// Filters the categories by either Expense or Income
router.post('/filter', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		categoryType: req.body.categoryType
	};
	
	CategoryController.filter(params).then(catType => res.send(catType));
})

module.exports = router;