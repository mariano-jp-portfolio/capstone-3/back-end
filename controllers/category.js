// Category Controller

// Category Schema
const Category = require('../models/Category');

// EXPORTS

// To create or add new category
module.exports.add = (params) => {
	let category = new Category({
		categoryType: params.categoryType,
		categoryName: params.categoryName,
		userId: params.userId
	});
	
	return category.save().then((category, err) => {
		return err ? false : true;
	});
};

// To create or add new records
module.exports.addRecord = (params) => {
	return Category.findById(params.categoryId).then(record => {
		record.records.push({
			description: params.description,
			amount: params.amount
		});
		
		return record.save().then((record, err) => {
			return err ? false : true;
		});
	});
};


// To fetch all of the user's categories
module.exports.getAll = (params) => {
	return Category.find({ userId: params.userId }).then(categories => categories);
};

// Filters the categories by either Expense or Income
module.exports.filter = (params) => {
	return Category.find({ userId: params.userId, categoryType: params.categoryType }).then(catType => catType);
};