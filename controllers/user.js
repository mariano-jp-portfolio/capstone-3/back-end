// User Controller

// User Schema
const User = require('../models/User');

// Authentication
const { OAuth2Client } = require('google-auth-library');
const auth = require('../auth');

// bcryptjs
const bcryptjs = require('bcryptjs');

// EXPORTS

// To check if user's email already exists in our DB
module.exports.emailExists = (body) => {
	return User.find({ email: body.email }).then(result => {
		return result.length > 0 ? true : false;
	})
};

// To register new user
module.exports.register = (body) => {
	let user = new User({
		givenName: body.givenName,
		familyName: body.familyName,
		email: body.email,
		password: bcryptjs.hashSync(body.password, 10),
		mobileNo: body.mobileNo
	})
	
	return user.save().then((user, err) => {
		return err ? false : true;
	});
};

// Login via email
module.exports.login = (body) => {
	return User.findOne({ email: body.email }).then(user => {
		if (user === null) {
			return { error: 'does-not-exist' };
		}
		
		if (user.loginType !== 'email') {
			return { error: 'login-type-error' };
		}
		
		const isPasswordMatched = bcryptjs.compareSync(body.password, user.password);
		
		if (isPasswordMatched) {
			// if password is matched, create a token that will contain the user's details
			return { accessToken: auth.createAccessToken(user.toObject()) };
		} else {
			return { error: 'incorrect-password' };
		}
	});
};

// Login via Google
module.exports.verifyGoogleTokenId = async (tokenId) => {
	const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID);
	
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: process.env.GOOGLE_CLIENT_ID
	});
	
	// If user's email is verified by Google, look if it already exists in our DB
	if (data.payload.email_verified === true) {
		const user = await User.findOne({ email: data.payload.email });
		
		// If the user is registered via Google, return an accessToken. Else, return an error
		if (user !== null) {
			if (user.loginType === 'google') {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			} else {
				return { error: 'login-type-error' };
			}
		} else {
			// If the user isn't registered yet, proceed with the registration
			let user = new User({
				givenName: data.payload.given_name,
				familyName: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'
			});
			
			// After saving their details in our DB, generate an accessToken for them
			return user.save().then((user, err) => {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			})
		}
	} else {
		// If user's Google email isn't verified
		return { error: 'google-auth-error' };
	}
};

// Fetching user's information
module.exports.getDetails = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined;
		return user;
	})
}

// To add a category
module.exports.addCategory = (params) => {
	return User.findById(params.userId).then(user => {
		user.categories.push({
			type: params.type,
			name: params.name
		});
		
		return user.save().then((user, err) => {
			return err ? false : true;
		});
	});
}

// To add a record
module.exports.addRecord = (params) => {
	return User.findById(params.userId).then(user => {
		user.records.push({
			type: params.type,
			name: params.name,
			description: params.description,
			amount: params.amount
		});
		
		return user.save().then((user, err) => {
			return err ? false : true;
		});
	});
}